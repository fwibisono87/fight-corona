from django.urls import path
from . import views

urlpatterns = [
    
    path('', views.olahraga, name='olahraga'),
    path('list/', views.listolahraga, name='list-olahraga'),

]