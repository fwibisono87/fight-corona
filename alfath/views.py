from django.shortcuts import render, redirect
from .models import Olahraga
from .forms import FormOlahraga

def olahraga(request):
    if request.method == 'POST':
        form = FormOlahraga(request.POST)
        if (form.is_valid()):
            olahraga = Olahraga()
            olahraga.nama_olahraga = form.cleaned_data['nama_olahraga']
            olahraga.save()
        return redirect('/list')
    else:
        form = FormOlahraga()
        return render(request, 'alfath/olahraga.html', {'form' : form})

def listolahraga(request):
    list_olahraga = Olahraga.objects.all()
    return render(request, 'alfath/listolahraga.html', {'list' : list_olahraga})



# Create your views here.
