from django import forms

class FormOlahraga(forms.Form):
    nama_olahraga = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'required': True,
                'placeholder': 'jenis olahraga'
            }
        )
    )