# FIGHT CORONA
_a PPW TK1 project_

Kelompok terdiiri atas:
* Francis Wibisono
* Farah Husna
* Alfath Fiqhya
* Tijani Putri
* Jati Langgeng

Asisted Dosen: Kak Intan Fadilla A.


## Feature List
Berikut merupakan feature list dan ceritanya, berdasarkan contributor
### `implemented` Francis Wibisono
Hiburan merupakan hal yang penting didapat selama pandemi ini. Oleh karena itu, fitur yang saya buat adalah sebuah _image board_.
Selain itu, saya juga membuat landing page project ini.

### `implemented` Farah Husna N.
Membuat aplikasi tentang edukasi Corona yang bertujuan untuk memberikan informasi kepada user tentang hal-hal penting yang berhubungan dengan corona.
Fitur yang dibuat adalah form yang dapat menampilkan list kegiatan yang dikerjakan selama pandemi.

### `implemented` Alfath Fiqhya
Membuat fitur olahraga yang menampilkan beberapa olahraga yang bisa dilakukan di dalam rumah.

### Tijani Putri
Fitur "rehat" atau resep sehat yang menampilkan beberapa resep makanan sehat beserta form feedback.

### `implemented` Jati Langgeng
Fitur "Maskermu" yang berguna untuk mengedukasi masyarakat tentang beberapa jenis masker dan kegunaannya, disertai form yang dapat diisi mengenai pengalaman pembaca tentang menggunakan masker.

## Where to find
This project can be found at

Gitlab (Private Repo)
* https://gitlab.com/fwibisono87/fight-corona

Heroku
* http://fight-corona.herokuapp.com/
* http://tk1.franciswibisono.com/

## Test coverage report
* `alfath: 90.1%`
* `farah: 100%`
* `francis: 96.9%`
* `tijani: NaN`
* `jati: 100%`
