# Generated by Django 3.1.2 on 2020-11-19 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('image', models.ImageField(blank=True, default='static/francis/img/no-image.png', upload_to='')),
                ('author', models.CharField(max_length=20)),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('caption', models.CharField(max_length=280)),
            ],
        ),
    ]
