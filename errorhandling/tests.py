from django.test import TestCase, Client
from .views import notFound

class ErrorTestClass(TestCase):
    def test_404(self):
        response = Client().get("/notfound")
        self.assertEqual(response.status_code, 404)

# Create your tests here.
