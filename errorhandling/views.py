from django.shortcuts import render


def notFound(request, exception=404):
    response = {}
    return render(request, 'global/404.html', status=404)
# Create your views here.
