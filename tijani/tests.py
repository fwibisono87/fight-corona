from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Tanggapan
from django.apps import apps
from .views import rehat, listTanggapan, menuPagi, menuSiang, menuMalam, makananRingan, minuman
from .forms import FormTanggapan
from .apps import TijaniConfig
# Create your tests here.

class ModelTest(TestCase):
    def setUp(self):
        self.tanggapan = Tanggapan.objects.create(list_tanggapan="Mudah dibuat")
       
    def test_instance_created(self):
        self.assertEqual(Tanggapan.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.tanggapan), "Mudah dibuat")

class UrlsTest(TestCase):
    def test_rehat_use_right_function(self):
        found = resolve('/rehat/')
        self.assertEqual(found.func, rehat)

    def test_listTanggapan_use_right_function(self):
        found = resolve('/rehat/listTanggapan/')
        self.assertEqual(found.func, listTanggapan)

    def test_menuPagi_use_right_function(self):
        found = resolve('/rehat/menuPagi/')
        self.assertEqual(found.func, menuPagi)
    
    def test_menuSiang_use_right_function(self):
        found = resolve('/rehat/menuSiang/')
        self.assertEqual(found.func, menuSiang)
    
    def test_menuMalam_use_right_function(self):
        found = resolve('/rehat/menuMalam/')
        self.assertEqual(found.func, menuMalam)

    def test_makananRingan_use_right_function(self):
        found = resolve('/rehat/makananRingan/')
        self.assertEqual(found.func, makananRingan)

    def test_minuman_use_right_function(self):
        found = resolve('/rehat/minuman/')
        self.assertEqual(found.func, minuman)

class ViewsTest(TestCase):
    def test_GET_rehat(self):
        response = Client().get('/rehat/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_GET_listTanggapan(self):
        response = Client().get('/rehat/listTanggapan/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'feedback.html')

    def test_POST_rehat_valid(self):
        response = Client().post('/rehat/',
                                    {
                                        'list_tanggapan': 'Mudah dibuat'
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_rehat_invalid(self):
        response = Client().post('/rehat/',
                                    {
                                        'list_tanggapan': ''
                                    }, follow=True)
        self.assertTemplateUsed(response, 'feedback.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(TijaniConfig.name, 'tijani')
        self.assertEqual(apps.get_app_config('tijani').name, 'tijani')