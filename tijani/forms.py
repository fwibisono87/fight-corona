from django import forms


class FormTanggapan(forms.Form):
    list_tanggapan = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Tulis tanggapanmu disini...',
                'type': 'text',
                'required': True
            }))
