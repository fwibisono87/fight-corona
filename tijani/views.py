from django.shortcuts import redirect, render
from .forms import FormTanggapan
from .models import Tanggapan

# Create your views here.
def rehat(request):
    form_tanggapan = FormTanggapan()
    if request.method == 'POST':
        form_fix = FormTanggapan(request.POST)
        if (form_fix.is_valid()):
            tanggapan = Tanggapan()
            tanggapan.list_tanggapan = form_fix.cleaned_data['list_tanggapan']
            tanggapan.save()
        return redirect('/rehat/listTanggapan')
    else:
        form = FormTanggapan()
        tanggapan = Tanggapan.objects.all()
        context = {
            'Form': form,
            'Tanggapan': tanggapan
        }
    return render(request, 'home.html', context)

def listTanggapan(request):
    tanggapan = Tanggapan.objects.all()
    return render(request, 'feedback.html', {'listTanggapan': tanggapan})

def menuPagi(request):
    return render(request, 'breakfast.html')

def menuSiang(request):
    return render(request, 'lunch.html')

def menuMalam(request):
    return render(request, 'dinner.html')

def makananRingan(request):
    return render(request, 'snacks.html')

def minuman(request):
    return render(request, 'beverages.html')

