from django.urls import path
from . import views

app_name = 'tijani'

urlpatterns = [
    path('', views.rehat, name='rehat'),
    path('listTanggapan/', views.listTanggapan, name='listTanggapan'),
    path('menuPagi/', views.menuPagi, name='menuPagi'),
    path('menuSiang/', views.menuSiang, name='menuSiang'),
    path('menuMalam/', views.menuMalam, name='menuMalam'),
    path('makananRingan/', views.makananRingan, name='makananRingan'),
    path('minuman/', views.minuman, name='minuman'),
]